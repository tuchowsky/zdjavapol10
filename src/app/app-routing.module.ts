import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageComponent } from './components/page/page.component';
import { TodoComponent } from './components/todo/todo.component';
import { MyOwnComponent } from './my-own-component/my-own.component';
import { MySecondComponentComponent } from './my-second-component/my-second-component.component';

// zdefiniowanie routes dla naszej aplikacji
const routes: Routes = [
  {path: '', component: PageComponent},
  {path: 'about', component: MySecondComponentComponent},
  {path: 'contact', component: MyOwnComponent},
  {path: 'todo', component: TodoComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
