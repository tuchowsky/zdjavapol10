import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MyFirstComponentComponent } from './my-first-component/my-first-component.component';
import { MySecondComponentComponent } from './my-second-component/my-second-component.component';
import { MyOwnComponent } from './my-own-component/my-own.component';
import { NavComponent } from './components/nav/nav.component';
import { LinkComponent } from './shared/link/link.component';
import { FormsModule } from '@angular/forms';
import { PageComponent } from './components/page/page.component';
import { ListItemComponent } from './components/list-item/list-item.component';
import { TodoComponent } from './components/todo/todo.component';

@NgModule({
  declarations: [
    AppComponent,
    MyFirstComponentComponent,
    MySecondComponentComponent,
    MyOwnComponent,
    NavComponent,
    LinkComponent,
    PageComponent,
    ListItemComponent,
    TodoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
