import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TodoService } from 'src/app/services/todo.service';

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss']
})
export class ListItemComponent implements OnInit {
  @Input() data;
  @Input() todo;
  @Input() index;
  @Output() outputName = new EventEmitter();
  
  updatedName: string = 'Brajan';

  constructor(private todoService: TodoService) { }

  ngOnInit(): void {
    // console.log(this.data);
  }
  onDataEmit() {
  // metoda zostanie wykonana gdy użytkownik kliknie w button
  // zostanie wyemitowany event z dziecka do rodzica
  // w pliku page.component.html mamy użycie tego komponentu, więc PageComponent
  // jest rodzicem wobec obecnego komponentu ListItemComponent
  // i właśnie tam zostanie 'wykonsologowna' wartość tego eventu przez metodę handleChange(event)
    this.outputName.emit(this.updatedName);
  }

  onDeleteTodo(index: number): void {
    this.todoService.deleteTodo(index);
  }
}
