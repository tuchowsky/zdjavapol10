import { Component, OnInit } from '@angular/core';
import { TodoInterface } from 'src/app/interfaces/todo.interface';
import { FirstServiceService } from 'src/app/services/first-service.service';
import { TodoService } from 'src/app/services/todo.service';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss']
})


export class PageComponent implements OnInit {
  isVisible: boolean = true;
  // zmienna dotycząca aktywności żółtego boxa
  isActive: boolean = false;

  namesArr: string[] = [
    'Bartek',
    'Dima',
    'Edyta',
    'Karolina'
  ];

  todoItemsArray: TodoInterface[] = [];

  // namesArray: Array<string> = [];

  constructor(
    private firstService: FirstServiceService,
    private todoService: TodoService
    ) { }

  ngOnInit(): void {
    // this.todoItemsArray = this.firstService.getTodoItems();
    // console.log(this.todoItemsArray);
    this.todoService.todoToSubscribe.subscribe(data => {
      this.todoItemsArray = data;
      console.log(data, 'to jest data');
    })
  }

  onButtonClick(): void {
    this.isVisible = !this.isVisible;
  }

  onElementActivate(): void {
    this.isActive = !this.isActive;
  }

  handleChange(event): void {
    console.log(event, 'emitted event');
  }
}
