import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { TodoService } from 'src/app/services/todo.service';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss']
})
export class TodoComponent implements OnInit {
  todoList = [];
  subscription: Subscription;

  constructor(private todoService: TodoService) { }

  ngOnInit(): void {
    this.subscription = this.todoService.todoToSubscribe.subscribe(data => {
      this.todoList = data;
    })
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
