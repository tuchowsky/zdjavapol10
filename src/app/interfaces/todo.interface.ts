export interface TodoInterface {
    description: string;
    date: Date;
}