import { Component, OnInit } from '@angular/core';

@Component({
  // po selektorze będziemy mogli odwoływać się do danego komponentu
  selector: 'app-my-first-component',
  templateUrl: './my-first-component.component.html',
  styleUrls: ['./my-first-component.component.scss']
})
export class MyFirstComponentComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {}

}
