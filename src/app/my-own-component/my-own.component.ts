import { Component } from '@angular/core';

@Component({
    selector: 'app-my-own',
    templateUrl: './my-own.component.html',
    styleUrls: ['./my-own.component.scss']
})

export class MyOwnComponent {
    name: string = 'Artur';
    yearsOld: number = 34;
    imageUrl: string = 'https://images.pexels.com/photos/4407291/pexels-photo-4407291.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260';

    newImageUrl: string = 'https://images.pexels.com/photos/414765/pexels-photo-414765.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260'
    

    onButtonClick(): void {
        let tempUrl: string=''; 
        tempUrl = this.imageUrl;
        this.imageUrl = this.newImageUrl; 

        this.newImageUrl = tempUrl;
    }
}