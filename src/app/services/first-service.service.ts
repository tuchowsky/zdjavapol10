import { Injectable } from '@angular/core';
import { TodoInterface } from '../interfaces/todo.interface';

@Injectable({
  providedIn: 'root'
})

export class FirstServiceService {
  todoArray: TodoInterface[] = [
    {
      description: 'My First Todo',
      date: new Date()
    },
    {
      description: 'My Second Todo',
      date: new Date()
    },
    {
      description: 'My Second Todo',
      date: new Date()
    }
  ];

  constructor() { }

  getTodoItems(): TodoInterface[] {
    return this.todoArray;
  }

}
