import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { TodoInterface } from '../interfaces/todo.interface';

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  todoArray: TodoInterface[] = [
    {
      description: 'My First Todo',
      date: new Date()
    },
    {
      description: 'My Second Todo',
      date: new Date()
    },
    {
      description: 'My Third Todo',
      date: new Date()
    }
  ];

  todoSubject$ = new BehaviorSubject(this.todoArray);
  todoToSubscribe = this.todoSubject$.asObservable();

  constructor() { }

  getTodoItems(): TodoInterface[] {
    return this.todoArray;
  }

  deleteTodo(i: number): void {
    this.todoArray.splice(i, 1);
    this.todoSubject$.next(this.todoArray);
  }
}
